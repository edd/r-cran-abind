abind (1.4-8-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 18 Sep 2024 12:20:16 -0500

abind (1.4-5-2) unstable; urgency=medium

  * Rebuilt for r-4.0 transition

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version
  * debian/control: Add Depends: on ${misc:Depends}
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/compat: Increase level to 10
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 15 May 2020 20:44:38 -0500

abind (1.4-5-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild for r-api-3.5 transition

 -- Andreas Tille <tille@debian.org>  Fri, 01 Jun 2018 15:13:58 +0200

abind (1.4-5-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild for r-api-3.4 transition

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 29 Sep 2017 18:52:07 +0200

abind (1.4-5-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 21 Jul 2016 17:58:44 -0500

abind (1.4-3-2) unstable; urgency=medium

  * debian/compat: Created			(Closes: #829660)
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 07 Jul 2016 05:25:16 -0500

abind (1.4-3-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 13 Mar 2015 07:44:36 -0500

abind (1.4-0-2) unstable; urgency=low

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Change Depends to ${R:Depends}
  * debian/control: Set Standards-Version: to current version 
  
  * (Re-)building with R 3.0.0 (beta)

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 30 Mar 2013 16:27:02 -0500

abind (1.4-0-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 27 Nov 2011 09:13:49 -0600

abind (1.3.0-2) unstable; urgency=low

  * Rebuilt for R 2.14.0 so that a default NAMESPACE file is created
  							(Closes: #646009)
  
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 20 Oct 2011 11:49:04 -0500

abind (1.3.0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 27 Feb 2011 13:23:05 -0600

abind (1.1.0-4) unstable; urgency=low

  * Rebuilt for R 2.10.0 to work with new R-internal help file conversion

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  
  * debian/control: Changed Section: to section 'gnu-r'
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 01 Nov 2009 08:03:18 -0600

abind (1.1.0-3) unstable; urgency=low

  * debian/rules: Simplified to cdbs-based one-liner sourcing r-cran.mk 
  * debian/control: Hence Build-Depends: updated to r-base-dev (>= 2.3.0)
  
  * debian/watch: Corrected regular expression with thanks to Rafael Laboissier
  * debian/post{inst,rm}: No longer call R to update html help index
  							(Closes: #369025)

  * debian/control: Standards-Version: increased to 3.7.2

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 12 Feb 2006 18:48:49 -0600

abind (1.1.0-2) unstable; urgency=low

  * Rebuilt under R 2.0.0
  * debian/control: Updated Build-Depends: and Depends: accordingly
  * debian/post{inst,rm}: Added to update html index
  * debian/watch: Added watch file

 -- Dirk Eddelbuettel <edd@debian.org>  Fri,  8 Oct 2004 17:36:33 -0500

abind (1.1.0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 16 Mar 2004 19:15:49 -0600

abind (1.0.1-1) unstable; urgency=low

  * Initial Debian Release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 10 Dec 2003 21:27:08 -0600


